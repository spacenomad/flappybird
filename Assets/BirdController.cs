using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
	[SerializeField]
	public PlayState playState;

	[SerializeField]
	private Rigidbody2D rigidbody;
	
	[SerializeField]
	private float JUMP_FORCE;
	[SerializeField]
	private float GRAVITY;
	private float ySpeed;

	public void Wake()
	{
		this.rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
	}
	

	public void Tick()
	{
	
		if(Input.GetKeyDown(KeyCode.Space))
		{
			ySpeed = JUMP_FORCE;
			GameManager.instance.sounds["jump"].Play();
		}
		else
		{
			ySpeed -= GRAVITY * Time.deltaTime;
		}
		
		rigidbody.velocity = new Vector2(0, ySpeed);
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		int DEATH = 7;
		int SCORE = 8;
		
		if(col.gameObject.layer == DEATH)
		{
			playState.birdDied();
		}
		else if(col.gameObject.layer == SCORE)
		{
			playState.birdScored();
			GameManager.instance.sounds["score"].Play();
		}
	}
}
