using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance {get; private set; }
	
	[Header ("Background")]
	[SerializeField]
	private GameObject background;
	private Material backgroundMat;
	[SerializeField]
	private float SCROLL_SPEED;
	private float offset;
	
	[Space]
	[Header ("Main Actors")]
	[SerializeField]
	public GameObject managers;
	[SerializeField]
	public GameObject entities;
	[SerializeField]
	public GameObject canvas;
	[SerializeField]
	public GameObject displayText;
	
	
	[Space]
	[SerializeField]
	//https://freesound.org/people/sonically_sound/sounds/624874/
	public AudioSource music;
	
	[Space]
	[Header ("Initialize for Dictionaries")]
	
	[SerializeField]
	BaseState[] statesToInitialize = new BaseState[3];
	
	[SerializeField]
	AudioSource[] soundsToInitialize = new AudioSource[3];
	
	
	
	public Dictionary<string, BaseState> states = new Dictionary<string, BaseState>();
	
	public Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();
	
	
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
		
		offset = 0;
	}
	
	void Start()
	{
		
		states.Add("start", statesToInitialize[0]);
		states.Add("play", statesToInitialize[1]);
		states.Add("lose", statesToInitialize[2]);
		
		
		sounds.Add("jump", soundsToInitialize[0]);
		sounds.Add("score", soundsToInitialize[1]);
		sounds.Add("death", soundsToInitialize[2]);
		
		
	
		StateMachine.instance.Change(states["start"], 0);
		backgroundMat = background.GetComponent<Renderer>().material;
	}

	void Update()
	{
		offset += (SCROLL_SPEED * Time.deltaTime);
		backgroundMat.SetTextureOffset("_MainTex", new Vector2(offset, 0));
		
		StateMachine.instance.Tick();
	}
	
	
	
}
