using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesController : MonoBehaviour
{
	[SerializeField]
	private float speed;
	
	public bool needsRemoving = false;
	
	private Vector2 pipesMovement;
	
	
	void Start()
	{
		pipesMovement = new Vector2(-1 * speed, 0);
	}
	
	
	public void Tick()
	{
		this.transform.Translate(pipesMovement * Time.deltaTime);
	}
	
	
}
