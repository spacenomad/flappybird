using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesSpawner : MonoBehaviour
{
	[SerializeField]
	private GameObject pipes;
	
	[System.NonSerialized]
	public bool spawning;
	
	[SerializeField]
	private float SPAWN_INTERVAL;
	
	
	[SerializeField]
	private List<PipesController> pipePairs = new List<PipesController> {};
	
	
	
	private float lastY;
	
	public void Wake()
	{
		lastY = transform.position.y;
		spawning = true;
		StartCoroutine(SpawnPipes());
	}
	
	public void Tick()
	{
	
		pipesBehavior();
		
	}
	
	
	void pipesBehavior()
	{
		
		
		if (spawning == false)
		{
			return;
		}
		
		
		foreach (PipesController pipePair in pipePairs)
		{
			pipePair.Tick();
			
			if (pipePair.transform.position.x < -15)
				pipePair.needsRemoving = true;
		}
		
		
		RemovePipes();
	}
	
	
	
	
	IEnumerator SpawnPipes() {
		while (spawning) {
			float y = Mathf.Clamp(lastY + Random.Range(-1, 1), this.transform.position.y - 4, this.transform.position.y + 4);
		
			PipesController pipePair = Instantiate(pipes, this.gameObject.transform).GetComponent<PipesController>();
			pipePair.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, y);
			pipePairs.Add(pipePair);
			
			
			yield return new WaitForSeconds(SPAWN_INTERVAL);
		}
	}
	
	void RemovePipes()
	{
		for (int index = 0; index < pipePairs.Count;)
		{
			if (!pipePairs[index].needsRemoving)
			{
				index++;
				continue;
			}
			
			Destroy(pipePairs[index].gameObject);
			pipePairs.Remove(pipePairs[index]);
		}
	
	}
	
	public void DestroyAllPipes()
	{
		foreach (PipesController pipePair in pipePairs)
		{
			Destroy(pipePair.gameObject);
		}
		
		pipePairs.Clear();
		
	}
}
