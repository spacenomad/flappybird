using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
	public static StateMachine instance {get; private set; }
	
	
	public BaseState currentState;
	private GameObject stateObject;
	
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	public void Change(BaseState newState, int pass)
	{
		currentState?.Exit();
		currentState = newState;
		Destroy(stateObject);
		stateObject = Instantiate(currentState.gameObject, this.transform);
		//currentState.gameObject.transform.parent = GameManager.instance.managers.transform;
		currentState.Enter(pass);
	}
	
	public void Tick()
	{
		currentState?.Tick();
	}
}
