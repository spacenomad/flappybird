using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState : MonoBehaviour
{
	public virtual void Enter(int passed) {}
	public virtual void Tick() {}
	public virtual void Exit() {}
}
