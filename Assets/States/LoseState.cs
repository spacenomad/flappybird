using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoseState : BaseState
{

	
	public override void Enter(int score)
	{
		GameManager.instance.sounds["death"].Play();
		GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "You lost, better luck next time! \n Your score is: " + score;
	}
	
	public override void Tick()
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			StateMachine.instance.Change(GameManager.instance.states["start"], 0);
		}
	}
}
