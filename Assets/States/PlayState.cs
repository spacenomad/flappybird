using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayState : BaseState
{

	[SerializeField]
	private GameObject birdReference;
	private GameObject bird;
	
	[SerializeField]
	private GameObject pipesSpawnerReference;
	private GameObject pipesSpawner;
	
	[SerializeField]
	private GameObject scoreReference;
	private GameObject scoreDisplay;

	bool pause;
	
	private int score;
	
	public override void Enter(int passed)
	{
		this.score = 0;
		GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "";
		
		SpawnActors();
	}
	
	public override void Tick()
	{

		if (bird != null)
			bird.GetComponent<BirdController>().Tick();
		if (pipesSpawner != null)
			pipesSpawner.GetComponent<PipesSpawner>().Tick();
	}
	
	public override void Exit()
	{
		DestroyActors();
	}
	
	void SpawnActors()
	{
		Transform birdParent = GameManager.instance.entities.transform;
		bird = Instantiate(birdReference, birdParent);
		bird.GetComponent<BirdController>().Wake();
		bird.GetComponent<BirdController>().playState = this;
		
		
		Transform spawnerParent = GameManager.instance.entities.transform;
		pipesSpawner = Instantiate(pipesSpawnerReference, spawnerParent);
		pipesSpawner.transform.position += new Vector3(pipesSpawner.transform.position.x + 10, pipesSpawner.transform.position.y, pipesSpawner.transform.position.z);
		pipesSpawner.GetComponent<PipesSpawner>().Wake();
		
		Transform scoreParent = GameManager.instance.canvas.transform;
		scoreDisplay = Instantiate(scoreReference, scoreParent);
		
	}
	
	void DestroyActors()
	{
		Destroy(bird);
		pipesSpawner.GetComponent<PipesSpawner>().spawning = false;
		pipesSpawner.GetComponent<PipesSpawner>().DestroyAllPipes();
		Destroy(pipesSpawner);
		Destroy(scoreDisplay);
	}
	
	public void birdDied()
	{
		StateMachine.instance.Change(GameManager.instance.states["lose"], score);
	}
	
	public void birdScored()
	{
		score++;
		scoreDisplay.GetComponent<TextMeshProUGUI>().text = score.ToString();
	}
}
