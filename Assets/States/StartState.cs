using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartState : BaseState
{
	public override void Enter(int passed)
	{
		GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "Welcome to GitaBird! \n Press enter to play.";
		GameManager.instance.music.Play();
	}
	
	public override void Tick()
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			StateMachine.instance.Change(GameManager.instance.states["play"], 0);
		}
	}
}
